# Octopeer
**Latest Build**

[![wercker status](https://app.wercker.com/status/99d460371fbff9688e3aafd8483808df/s "wercker status")](https://app.wercker.com/project/bykey/99d460371fbff9688e3aafd8483808df)
[![Coverage Status](https://coveralls.io/repos/bitbucket/CasBs/octopeer/badge.svg?branch=develop)](https://coveralls.io/bitbucket/CasBs/octopeer?branch=develop)
[![Dependency Status](https://www.versioneye.com/user/projects/5772b719752cd1004c59520c/badge.svg?style=flat-square)](https://www.versioneye.com/user/projects/5772b719752cd1004c59520c)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/eeff19a71a464637bb6913642c8985fe)](https://www.codacy.com/app/s-j-m-buijs/octopeer?utm_source=CasBs@bitbucket.org&amp;utm_medium=referral&amp;utm_content=CasBs/octopeer&amp;utm_campaign=Badge_Grade)

[![wercker status](https://app.wercker.com/status/99d460371fbff9688e3aafd8483808df/m "wercker status")](https://app.wercker.com/project/bykey/99d460371fbff9688e3aafd8483808df)

**Latest Release**

[![Coverage Status](https://coveralls.io/repos/bitbucket/CasBs/octopeer/badge.svg?branch=master)](https://coveralls.io/bitbucket/CasBs/octopeer?branch=master)
[![Dependency Status](https://www.versioneye.com/user/projects/5772b780752cd100485b855b/badge.svg?style=flat-square)](https://www.versioneye.com/user/projects/5772b780752cd100485b855b)

**This project works in combination with the following server**

[The Octopeer Server](https://github.com/theSorcerers/octopeer)

## Setting up / building
Install *NodeJS* from [here](https://nodejs.org)

Make sure *gulp* is globally installed, if not: install using npm:
```
npm install -g gulp
```
Also, in order to compile things, get *typings*:
```
npm install -g typings
```
And use it to install the typings for _chrome_ and _jasmine_:
```
typings install
```

Install other dependencies:
```
npm install
```

## Gulp
A few commands can be executed using gulp:

- *Compile* convert typescript to javascript
- *Test*: Run the test suite
- *Typescript-lint*: Run the typescript style-checker
- *Css-lint*: Run the css style-checker
- *Html-lint*: Run the html style-checker
- *Build*: Build the code, it converts .ts files into .js files

You can view the code coverage as measured in `target/assets/unit-test-coverage/html-report`

### Using the result
Load the `dest` folder in chrome:

- Go to `extensions` (or your regional equivalent)
- Make sure `Developer Mode` is enabled
- Load unpacked extension
- Choose `dest` folder

# Software Engineering Documentation

## Other documentation

- Class Diagram, 17-06-2016: [here](https://drive.google.com/file/d/0Byx_cnrHIK23MVEtWkh2cFdaMXc/view?usp=sharing)
- Coding Choices Motivation, 17-06-2016: [here](https://bitbucket.org/CasBs/ooc-octopeer/src/14d117094e215ca6085b61959040aaa534621ac9/doc/Architecture/Coding%20Choices%20Clarification.pdf?at=release%2Fsprint-8&fileviewer=file-view-default)
- Tooling Choices Motivation, 20-05-2016: [here](https://bitbucket.org/CasBs/ooc-octopeer/src/1dfb0a9efb1f193434ee81f8fc007b321540fadd/doc/Architecture/Tooling%20Choices%20Clarification.pdf?at=release%2Fsprint-5&fileviewer=file-view-default)