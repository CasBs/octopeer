/// <reference path="./interfaces/Message.ts" />
/// <reference path="./interfaces/SessionDataGatherer.ts" />
/// <reference path="./interfaces/TrackingCollector.ts" />
/// <reference path="./interfaces/DatabaseSchemes/SessionJSON.ts" />
/// <reference path="./OctopeerConstants.ts" />

/// <reference path="../../../typings/index.d.ts" />
/// <reference path="./Main.d.ts" />

/**
 * ChromeTrackingCollector
 * Collects all the tracking data and multiplexes it over a singular chrome port.
 */
export class ChromeTrackingCollector implements TrackingCollector {
    
    private session: SessionJSON;
    private port: chrome.runtime.Port;

    constructor (sc: SessionDataGatherer) {
        this.session = sc.getSessionData();
        
        this.port = chrome.runtime.connect({
            name: OCTOPEER_CONSTANTS.chrome_message_sender_id
        });
    }

    /**
     * Indicates whether the session is instantiated and the collector can send messages.
     * @returns {boolean} 
     */
    public isReadyToSend(): boolean {
        return this.session != null;
    }

    /**
     * Send a tracker message using the ChromeTrackingCollector.
     */
    public sendMessage(message: Message): void {
        if (this.session == null) {
            throw new Error("Session was not initialized before sending data.");
        }
        
        message.data.session = this.session;
        this.port.postMessage(message);
    }
}

main.declareTrackingCollector((sessionDataGatherer) =>
    new ChromeTrackingCollector(sessionDataGatherer));