/**
 * The data tag format of the BitBucket pull request.
 */
interface BitBucketPullRequest {

    /**
     * The localID of the pull request.
     */
    localId: number;

    /**
     * The author of the pull request.
     */
    author: {
        username: string;
    };
}